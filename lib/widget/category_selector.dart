import 'package:flutter/material.dart';

class CategorySelector extends StatefulWidget {
  @override
  _CategorySelectorState createState() => _CategorySelectorState();
}

class _CategorySelectorState extends State<CategorySelector> {
  int selectIndex = 0;

  // make list with response varibale string
  final List<String> categories = ['Messages', 'Online', 'Groups', 'Requests'];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65.0,
      color: Theme.of(context).primaryColor,
      // make menu from list view
      child: ListView.builder(
          // make orientation vertical
          scrollDirection: Axis.horizontal,
          itemCount: categories.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              // membuat fungsi ketika di klik
              onTap: () {
                setState(() {
                  selectIndex = index;
                });
              },
              child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                  child: Text(
                    // textSet
                    categories[index],
                    // setStyle
                    style: TextStyle(
                        // set color ketika di klik maka akan engubah putih
                        color: index == selectIndex
                            ? Colors.white
                            : Colors.white60,
                        // set size
                        fontSize: 20.0,
                        // set weight
                        fontWeight: FontWeight.bold,
                        // add linespacing
                        letterSpacing: 0.5),
                  )),
            );
          }),
    );
  }
}
