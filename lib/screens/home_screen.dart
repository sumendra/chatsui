import 'package:flutter/material.dart';
import 'package:flutter_chat_ui/widget/category_selector.dart';
import 'package:flutter_chat_ui/widget/favorite_contatct.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // change background use red color
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        // make menu drawer
        leading: IconButton(
          icon: Icon(Icons.menu),
          iconSize: 30.0,
          color: Colors.white,
          onPressed: () {},
        ),
        // pluss text for android toolbar
        title: Text('Chats',
            style: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold)),
        elevation: 0.0,
        // make menu search
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            iconSize: 30.0,
            color: Colors.white,
            onPressed: () {},
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          // to make selector in top of list friend
          CategorySelector(
              // make menu
              ),
          Expanded(
            // make container to temp of list
            child: Container(
              height: 500.0,
              decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0))),
              child: Column(
                // call row for favoriteContact
                children: <Widget>[FavoriteContact()],
              ),
            ),
          )
        ],
      ),
    );
  }
}
